#ifndef _ACTIVE_OBJECTS_H_
#define _ACTIVE_OBJECTS_H_

#include "qpcpp.h"
#include "lwip/ip_addr.h"

enum Signals
{
    TIMEOUT_SIG = QP::Q_USER_SIG, // timeout to blink LED
    DATAGRAM_RECEIVED_SIG,        // from lwIP to an AO about a received UDP msg

    MAX_PUB_SIG,                                   // the last published signal

    SEND_UDP_SIG,       // posted directly to lwIPMgr to send text via UDP
    LWIP_RX_READY_SIG,  // posted by Ethernet driver ISR directly to lwIPMgr
    LWIP_TX_READY_SIG,  // posted by Ethernet driver ISR directly to lwIPMgr
    LWIP_SLOW_TICK_SIG, // slow tick signal for LwIP manager

    MAX_SIG
};

#define MAX_DATAGRAM_PAYLOAD_LEN (1024)
struct DatagramEvt : public QP::QEvt {
    ip_addr_t address;
    uint16_t port;
    uint16_t payload_length;
    uint8_t payload[MAX_DATAGRAM_PAYLOAD_LEN];   // text to deliver
};

// active object(s) used in this application -------------------------------
extern QP::QActive * const AO_LwIPMgr;// "opaque" pointer to LwIPMgr AO
extern QP::QActive * const AO_EchoServer;// "opaque" pointer to LwIPMgr AO
extern QP::QActive * const AO_Blinker;
extern QP::QTicker * const AO_Ticker;

#endif
