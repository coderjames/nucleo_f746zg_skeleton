#include "active_objects.h"
#include "qpcpp.h"    // QP/C++ API
#include <cstring>

using namespace QP;

class EchoServer : public QActive
{
   public:
      EchoServer();    // ctor

   private:
      static QState initial(EchoServer *me, QEvt const *e);
      static QState running(EchoServer *me, QEvt const *e);
};

// Local objects -------------------------------------------------------------
static EchoServer l_EchoServer;    // the single instance of LwIPMgr AO

// Global-scope objects ------------------------------------------------------
QActive *const AO_EchoServer = (QActive *)&l_EchoServer;    // "opaque" pointer

EchoServer::EchoServer() : QActive((QStateHandler)&EchoServer::initial) {}
//............................................................................
QState EchoServer::initial(EchoServer *me, QEvt const *e)
{
   return Q_TRAN(&EchoServer::running);
}

QState EchoServer::running(EchoServer *me, QEvt const *e)
{
   switch (e->sig)
   {
      /*
      case Q_ENTRY_SIG:
      {
         // register with lwIPMgr for data on UDP/777
         return Q_HANDLED();
      }
      */

      /*
      case Q_EXIT_SIG:
      {
         // unregister with lwIPMgr
         return Q_HANDLED();
      }
      */

      case DATAGRAM_RECEIVED_SIG:
      {
         DatagramEvt const *request = (DatagramEvt const *)e;
         
         DatagramEvt *response = Q_NEW(DatagramEvt, SEND_UDP_SIG);
         response->port = request->port;
         response->address = request->address;
         response->payload_length = 3 + request->payload_length;
         response->payload[0] = 'R';
         response->payload[1] = 'P';
         response->payload[2] = ':';
         std::memcpy(&(response->payload[3]), request->payload, request->payload_length);
         AO_LwIPMgr->POST(response, AO_EchoServer);
         
         return Q_HANDLED();
      }

      default:
         return Q_SUPER(&QHsm::top);
   }
}