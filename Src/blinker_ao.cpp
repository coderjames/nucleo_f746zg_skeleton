
#include "qpcpp.h"  // QP/C++ API
#include "active_objects.h"

using namespace QP;

class Blinker : public QActive {
public:
    Blinker();                                                         // ctor

private:
    static QState initial(Blinker *me, QEvt const *e);
    static QState running(Blinker *me, QEvt const *e);
    QTimeEvt m_te_blink;
};


// Local objects -------------------------------------------------------------
static Blinker l_Blinker;                 // the single instance of EchoServer AO

// Global-scope objects ------------------------------------------------------
QActive * const AO_Blinker = (QActive *)&l_Blinker;        // "opaque" pointer

Blinker::Blinker()
    : QActive((QStateHandler)&Blinker::initial),
      m_te_blink(TIMEOUT_SIG)
{
}
//............................................................................
QState Blinker::initial(Blinker *me, QEvt const *e)
{
    // perform task-specific initialization here
    return Q_TRAN(&Blinker::running);
}

QState Blinker::running(Blinker *me, QEvt const *e) {
    switch (e->sig)
    {
        case Q_ENTRY_SIG:
        {
            me->m_te_blink.postEvery((QActive *)me, 500);
            HAL_GPIO_WritePin(LD2_BLUE_GPIO_Port, LD2_BLUE_Pin, GPIO_PIN_RESET);
            return Q_HANDLED();
        }
        
        case Q_EXIT_SIG:
        {
            me->m_te_blink.disarm();
            return Q_HANDLED();
        }

        case TIMEOUT_SIG:
        {
           HAL_GPIO_TogglePin(LD2_BLUE_GPIO_Port, LD2_BLUE_Pin);
           return Q_HANDLED();
        }
        
        /*
        case MY_OWN_SIG:
        {
            //MyEvt const *event = (MyEvt const *)e;
            return Q_HANDLED();
        }
        */

        default:
            return Q_SUPER(&QHsm::top);
    }
}
