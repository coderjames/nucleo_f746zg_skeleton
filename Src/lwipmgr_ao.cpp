//****************************************************************************
// Product: lwIP-Manager Active Object
// Last Updated for Version: 5.4.0
// Date of the Last Update:  2015-05-12
//
//                    Q u a n t u m     L e a P s
//                    ---------------------------
//                    innovating embedded systems
//
// Copyright (C) Quantum Leaps, LLC. All rights reserved.
//
// This program is open source software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Alternatively, this program may be distributed and modified under the
// terms of Quantum Leaps commercial licenses, which expressly supersede
// the GNU General Public License and are specifically designed for
// licensees interested in retaining the proprietary status of their code.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// Contact information:
// Web  : https://state-machine.com
// Email: info@state-machine.com
//****************************************************************************
#define LWIP_ALLOWED

#include "qpcpp.h"  // QP/C++ API
//#include "dpp.h"    // application events and active objects
//#include "bsp.h"    // Board Support Package
#include "active_objects.h"

#include "lwip.h"   // lwIP stack
#include "lwip/udp.h"
//#include "httpd.h"  // lwIP application

#include <string.h>
#include <stdio.h>

using namespace QP;

Q_DEFINE_THIS_FILE


//#define FLASH_USERREG0          (*(uint32_t const *)0x400FE1E0)
//#define FLASH_USERREG1          (*(uint32_t const *)0x400FE1E4)
#if LWIP_TCP
#define LWIP_SLOW_TICK_MS       TCP_TMR_INTERVAL
#else
#define LWIP_SLOW_TICK_MS       10
#endif

// Active object class -------------------------------------------------------
class LwIPMgr : public QActive {

    QTimeEvt m_te_LWIP_SLOW_TICK;
    struct netif   *m_netif;
    struct udp_pcb *m_upcb;
    uint32_t        m_ip_addr;     // IP address in the native host byte order

#if LWIP_TCP
    uint32_t m_tcp_tmr;
#endif
#if LWIP_ARP
    uint32_t m_arp_tmr;
#endif
#if LWIP_DHCP
    uint32_t m_dhcp_fine_tmr;
    uint32_t m_dhcp_coarse_tmr;
#endif
#if LWIP_AUTOIP
    uint32_t m_auto_ip_tmr;
#endif

public:
    LwIPMgr();                                                         // ctor

private:
    static QState initial(LwIPMgr *me, QEvt const *e);
    static QState running(LwIPMgr *me, QEvt const *e);
};


// Local objects -------------------------------------------------------------
static LwIPMgr l_lwIPMgr;                 // the single instance of LwIPMgr AO

// Global-scope objects ------------------------------------------------------
QActive * const AO_LwIPMgr = (QActive *)&l_lwIPMgr;        // "opaque" pointer

/* utilities added by QL ...................................................*/
/**
* Allocate a transport-layer pbuf and copies the provided data buffer 'data'
* of length 'len' bytes into the payload(s) of the pbuf. The function takes
* care of splitting the data into successive pbuf payloads, if necessary.
*
* The function returns the newly created pbuf or NULL if the pbuf cannot
* be allocated.
*/
struct pbuf *pbuf_new(const u8_t *data, u16_t len) {
    struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_RAM);
    struct pbuf *q = p;
    while ((q != (struct pbuf *)0) && (len >= q->len)) {
        memcpy(q->payload, data, q->len);         /* copy data into payload */
        len  -= q->len;                                 /* remaining length */
        data += q->len;                              /* remainig data chunk */
        q = q->next;                                       /* get next pbuf */
    }
    return p;
}
// UDP handler ...............................................................
static void udp_rx_handler(void *arg, struct udp_pcb *upcb,
                           struct pbuf *p, const struct ip4_addr *addr, u16_t port);

//............................................................................
LwIPMgr::LwIPMgr()
    : QActive((QStateHandler)&LwIPMgr::initial),
      m_te_LWIP_SLOW_TICK(LWIP_SLOW_TICK_SIG)
{}
//............................................................................
QState LwIPMgr::initial(LwIPMgr *me, QEvt const *e) {
    MX_LWIP_Init();

    me->m_upcb = udp_new();
    udp_bind(me->m_upcb, IP_ADDR_ANY, 777);  // use port 777 for UDP echo server
    udp_recv(me->m_upcb, &udp_rx_handler, me);

    return Q_TRAN(&LwIPMgr::running);
}
//............................................................................
QState LwIPMgr::running(LwIPMgr *me, QEvt const *e) {
    switch (e->sig) {
        case Q_ENTRY_SIG: {
            me->m_te_LWIP_SLOW_TICK.postEvery((QActive *)me,
                (LWIP_SLOW_TICK_MS * 1000) / 1000);
            return Q_HANDLED();
        }
        case Q_EXIT_SIG: {
            me->m_te_LWIP_SLOW_TICK.disarm();
            return Q_HANDLED();
        }

        case SEND_UDP_SIG: {
            DatagramEvt const *de = (DatagramEvt const *)e;
            struct pbuf *p = pbuf_new(de->payload, de->payload_length);
            if (p != (struct pbuf *)0) {
                udp_sendto(me->m_upcb, p, &(de->address), de->port);
                pbuf_free(p);                      // don't leak the pbuf!
            }
            return Q_HANDLED();
        }

        case LWIP_RX_READY_SIG: {
            eth_driver_read();
            return Q_HANDLED();
        }
/*
        case LWIP_TX_READY_SIG: {
            eth_driver_write();
            return Q_HANDLED();
        }
*/
        case LWIP_SLOW_TICK_SIG: {
            sys_check_timeouts();
/*
            if (me->m_ip_addr != me->m_netif->ip_addr.addr) {
                me->m_ip_addr = me->m_netif->ip_addr.addr; // save the IP addr
                uint32_t ip_net  = ntohl(me->m_ip_addr);// IP in network order
                       // publish the text event to display the new IP address
                TextEvt *te = Q_NEW(TextEvt, DISPLAY_IPADDR_SIG);
                snprintf(te->text, Q_DIM(te->text), "%d.%d.%d.%d",
                         ((ip_net) >> 24) & 0xFF,
                         ((ip_net) >> 16) & 0xFF,
                         ((ip_net) >> 8)  & 0xFF,
                         ip_net           & 0xFF);
                QF::PUBLISH(te, me);
            }
*/

#if LWIP_TCP
            me->m_tcp_tmr += LWIP_SLOW_TICK_MS;
            if (me->m_tcp_tmr >= TCP_TMR_INTERVAL) {
                me->m_tcp_tmr = 0;
                tcp_tmr();
            }
#endif
#if LWIP_ARP
            me->m_arp_tmr += LWIP_SLOW_TICK_MS;
            if (me->m_arp_tmr >= ARP_TMR_INTERVAL) {
                me->m_arp_tmr = 0;
                etharp_tmr();
            }
#endif
#if LWIP_DHCP
            me->m_dhcp_fine_tmr += LWIP_SLOW_TICK_MS;
            if (me->m_dhcp_fine_tmr >= DHCP_FINE_TIMER_MSECS) {
                me->m_dhcp_fine_tmr = 0;
                dhcp_fine_tmr();
            }
            me->m_dhcp_coarse_tmr += LWIP_SLOW_TICK_MS;
            if (me->m_dhcp_coarse_tmr >= DHCP_COARSE_TIMER_MSECS) {
                me->m_dhcp_coarse_tmr = 0;
                dhcp_coarse_tmr();
            }
#endif
#if LWIP_AUTOIP
            me->auto_ip_tmr += LWIP_SLOW_TICK_MS;
            if (me->auto_ip_tmr >= AUTOIP_TMR_INTERVAL) {
                me->auto_ip_tmr = 0;
                autoip_tmr();
            }
#endif
            return Q_HANDLED();
        }
        /*
        case LWIP_DRIVER_GROUP + LWIP_RX_OVERRUN_OFFSET: {
            LINK_STATS_INC(link.err);
            return Q_HANDLED();
        }
        */
    }
    return Q_SUPER(&QHsm::top);
}


// UDP receive handler -------------------------------------------------------
static void udp_rx_handler(void *arg, struct udp_pcb *upcb,
                           struct pbuf *p, const struct ip4_addr *addr, u16_t port)
{
  //TextEvt *te = Q_NEW(TextEvt, DISPLAY_UDP_SIG);
  //strncpy(te->text, (char *)p->payload, Q_DIM(te->text));
  //QF::PUBLISH(te, AO_LwIPMgr);

  if (p->tot_len <= MAX_DATAGRAM_PAYLOAD_LEN)
  {
    DatagramEvt *de = Q_NEW(DatagramEvt, DATAGRAM_RECEIVED_SIG);
    
    de->port = port;
    de->address.addr = addr->addr;

    // TODO: handle chained pbufs
    de->payload_length = p->len;
    memcpy(de->payload, p->payload, p->len);

    AO_EchoServer->POST(de, AO_LwIPMgr);
  }
  pbuf_free(p);                                      // don't leak the pbuf!
}
